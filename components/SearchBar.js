import React,{Component} from 'react';
import {View, TextInput, StyleSheet} from 'react-native';

export default class SearchBar extends Component{
    constructor(props){
        super(props);
        this.state = {
            value: 'hello'
        }
    }
    search(text="iraq"){
        this.setState({
            value: text
        });
        this.props.searchFun(text);
        console.log('testing',text);
    }
    render(){
        return(
            <View style={styles.searchBar}>
                <TextInput placeholder = "Search" onChangeText={this.search.bind(this)} value={this.state.value}/>
            </View>
        );
    }
}

const styles = StyleSheet.create ({
    searchBar: {
        flex: 0.15,
        padding: 10,
        height:100,
        margin: 1
    }
});