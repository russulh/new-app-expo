import React,{Component} from 'react';
import {View} from 'react-native';
import NewItem from './NewItem';

export default class List extends Component{
    constructor(props){
        super(props);
        this.state = {
            news: props.news
        }
    }

    render(){
        const {news} = this.props;
        const newList = news.map((newsItemData, i)=> {
            <NewItem key={i} item={newsItemData}/>
        });
        return(
            <View style={{flex: 1, backgroundColor:"gray"}}>{newList}</View>
        )
    }
}