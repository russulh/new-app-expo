import React,{Component} from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';

export default class NewItem extends Component{
    render(){
        let {item} = this.props;
        const {description, publishedAt, title, urlToImage} = item;
        console.log(item);
        return (
            <View style={styles.headerContainer}>
                <Image style={styles.image} resizeMode="cover" source={{url:urlToImage}}/>

                <View>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.title}>{description}</Text>
                    <Text style={styles.title}>{publishedAt}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    headerContainer: {
        height: 60,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#ECF0F1",
        justifyContent: "center",
        textTransform: "uppercase"
    },
    image: {
        height: 40,
        width: 95
    },
    title: {
        marginLeft: 20,
        fontSize:20
    }
});