import React, {Component} from 'react';
import {View} from 'react-native';
import {Constants} from 'expo';

import Header from '../components/Header';
import SearchBar from '../components/SearchBar';
import List from '../components/List'

export default class Main extends Component {
    constructor(props){
        super(props);
        this.state = {
            news:[]
        };
    }
    
    componentDidMount(){
        fetch('https://newsapi.org/v2/everything?q=iraq&sortBy=publishedAt&apiKey=159ab043c8be4a0d9f4f385a853ef619')
            .then((res)=> res.json())
            .then(news =>{
                // console.log('news',news);
                this.setState({
                    news:news.articles
                });
            })
            .catch((err)=>{
                console.log(err);
            });
    }

    search(q){
        fetch(`https://newsapi.org/v2/everything?q=${q}&sortBy=publishedAt&apiKey=159ab043c8be4a0d9f4f385a853ef619`)
            .then((res)=> res.json())
            .then(news =>{
                // console.log('news',news);
                this.setState({
                    news:news.articles
                });
            })
            .catch((err)=>{
                console.log(err);
            });
    }


    render() {
        return (
            <View style={{flex:1, marginTop:Constants.statusBarHeight}}>
                <Header/>
                <SearchBar searchFun={this.search.bind(this)}/>
                <List news={this.state.news}/>
            </View>
        );
    }
}